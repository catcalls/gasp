Option Explicit On
Imports System.IO

Module Module1


    REM DO NOT GENERATE NEW CODE (ITLL BE JUNK)
    REM INSTEAD, ANALYSE EXISTING CODE SOURCE FILES


    'problem with dim x,y as integer in parser

    'run recursive search of module1.vb and form1.vb and *.vb files in func analysis

    'example of supervised top down narrowing/condescending parse
    Sub Main()

        Dim inpute As Integer = 0
        While True
            Console.Clear()

            inpute = 0
            '-------------------------------------------------------------
            Console.WriteLine("1. OPEN Parser.")
            Console.WriteLine("2. CREATE typeLst")
            Console.WriteLine("3. FUNCTION analysis")
            Console.WriteLine("----------------------------------")
            '-------------------------------------------------------------
            Console.WriteLine("4. TOKEN analysis")
            Console.WriteLine("5. WHILE LOOP analysis")
            Console.WriteLine("6. IF...ELSE...END IF analysis")
            Console.WriteLine("----------------------------------")
            '-------------------------------------------------------------
            Console.WriteLine("10. EXIT")
            '-------------------------------------------------------------
            Console.WriteLine("Select a number from the list above by entering it in on your keyboard, then press return.")
            Try
                Console.WriteLine()
                inpute = Console.ReadLine()
            Catch ex As Exception
                Console.WriteLine(ex.ToString)
            End Try

            If inpute = 10 Then
                Exit Sub
            End If
            If inpute = 0 Then
                Console.Write(":/")
            End If
            If inpute = 1 Then
                parser()
            End If
            If inpute = 2 Then
                typeList()
            End If
            If inpute = 3 Then
                FunctionAnalysis()
            End If
            If inpute = 4 Then
                ' FOR LOOP ANALYSIS

                'dim double for result analysis
                Dim d As Double = 0.0

                d = ForLoopAnalysis()
                Console.WriteLine("= " & d.ToString)
                Console.ReadLine()
            End If
        End While




        '        Console.ReadLine()
    End Sub
    Public FileArray(1000000) As String

    Public Function ForLoopAnalysis() As Double


        'for var dec2 type equates (=) Number dec3 Number
        Dim result As Double = 0.0
        Dim total As Double = 0.0


        Dim inpute As Integer
        Console.WriteLine("1.Recursive Search")
        Console.WriteLine("2. Single File")
        Try
            inpute = Console.ReadLine()
        Catch ex As Exception

            Console.WriteLine("Input a number [1..2]")
        End Try

        If inpute = 1 Then

            'recursive *.vb search array builder here
            Dim index = 0
            Dim lngth As Integer = SearchDirectory(New DirectoryInfo("C:\Users\g_obr\OneDrive\Documents\Visual Studio 2008\Projects"))
            'loop on fileArray() here
      
            While index <= lngth
                If Not FileArray(index) Is Nothing Then
                    If Not FileArray(index).Length = 0 Then
                        result += forLoop(FileArray(index))
                        Console.WriteLine("On File " + index.ToString + " Out of " + lngth.ToString)
                        total += 1
                    End If
                End If
                index += 1
            End While
        ElseIf inpute = 2 Then
            Console.WriteLine(":/ (Module1.vb)")
            Dim strFile As Object = Console.ReadLine()
            forLoop(strFile)

        End If
        Console.Write("Result: " & result.ToString & " Total: " & total.ToString & " = ")
        Return result / total


    End Function
    Private Function forLoop(ByVal ModFile1 As Object) As Double

        Dim result As Double = 1
        Dim sir0 As StreamReader = File.OpenText(ModFile1)

        'analyse file contents extracting FOR LOOPS according to grammar
        'for loop grammar
        '
        '
        'match FOR and closing block NEXT
        'generate array of all text+char positions

        'declare string array + array index (dexter)

        Dim inputeAnalysis(10000) As String
        Dim dexter As Integer = 0
        While Not sir0.EndOfStream

            inputeAnalysis(dexter) = sir0.ReadLine()
            dexter += 1
        End While
        '        Reset file pointer
        dexter = 0
        'declare class node

        Dim n(1000) As node

        Dim CharPosition As Integer = 0
        Dim Node As String = "FAIL"
        Dim enter As String = "NO"
        Dim nest As Integer = 0
        For Each Node In inputeAnalysis
            If Not Node Is Nothing Then

                'reset charposition
                CharPosition = 0
                Dim tempStrings(1000000) As String
                Dim tempChars() As Char
                Dim tempDexter As Integer = 0

                tempChars = Node.ToArray
                For i As Integer = 0 To tempChars.Length - 1

                    If Not tempChars(i) = " " Then
                        tempStrings(tempDexter) += tempChars(i)
                    End If

                    Try

                        If tempChars(i) = " " Or i = tempChars.Length - 1 Then
                            If Not tempStrings(tempDexter) Is Nothing Then
                                n(dexter) = New node

                                n(dexter).Insert(tempStrings(tempDexter))
                                n(dexter).SetPosition(tempDexter)
                                n(dexter).Line = dexter

                                dexter += 1
                                tempDexter += 1
                                '                Console.WriteLine(n(dexter - 1).Extract())

                            Else
                                '                 Console.WriteLine("DEXTERS DEAD")
                            End If

                        End If
                    Catch ex As Exception
                        result = 0
                    End Try

                Next


                '   Console.WriteLine()
            End If

        Next

        'close file Module1.vb
        sir0.Close()
        ' Console.ReadLine()
        'reset dexter
        dexter = 0
        Dim sue0 As StreamWriter = File.AppendText(".\DB_TOKEN.txt")

        '        Console.WriteLine("Writing Nodes to DB_TOKEN.txt")
        While dexter < 1000
            Try

                If Not n(dexter) Is Nothing Then
                    'Console.WriteLine("NODE" + dexter.ToString)
                    sue0.WriteLine("NODE " + dexter.ToString)
                    'Console.WriteLine(n(dexter).returnPosition().ToString + " = START")
                    'Console.WriteLine(n(dexter).Line.ToString)
                    sue0.WriteLine("CP:" + n(dexter).returnPosition.ToString + ":LINE:" + n(dexter).Line.ToString)
                    'Console.WriteLine("TOKEN:" + n(dexter).Extract())
                    sue0.WriteLine("TOKEN:" + n(dexter).Extract())
                End If

                dexter += 1
            Catch ex As Exception
                result = 0
            End Try

        End While
        sue0.Flush()
        sue0.Close()

        'Console.ReadLine()
        Return result
    End Function

    Public index As Integer = 0
    Private Function SearchDirectory(ByVal dir As DirectoryInfo) As Integer


        Dim File As FileInfo
        For Each File In dir.GetFiles()
            If File.Extension = ".vb" Then
                FileArray(index) = File.FullName
                Console.WriteLine(FileArray(index))
                index += 1
                ' Console.ReadLine()
            End If
        Next

        Dim DirItem As DirectoryInfo

        For Each DirItem In dir.GetDirectories
            Try
                SearchDirectory(DirItem)
            Catch ex As Exception

            End Try
        Next
        Return index
    End Function
    Sub FunctionAnalysis()

        Dim inpute As Integer
        Console.WriteLine("1.Recursive Search")
        Console.WriteLine("2. Single File")
        Try
            inpute = Console.ReadLine()
        Catch ex As Exception

            Console.WriteLine("Input a number [1..2]")
        End Try

        If inpute = 1 Then

            'recursive *.vb search array builder here
            Dim index = 0
            SearchDirectory(New DirectoryInfo("C:\Users\g_obr\OneDrive\Documents\Visual Studio 2008\Projects"))
            'loop on fileArray() here

            While index <= FileArray.Length - 1
                If Not FileArray(index) Is Nothing Then
                    If Not FileArray(index).Length = 0 Then
                        funcList(FileArray(index))
                    End If
                End If
                index += 1
            End While
        ElseIf inpute = 2 Then
            Console.WriteLine(":/ (Module1.vb)")
            Dim strFile As Object = Console.ReadLine()
            funcList(strFile)

        End If
        Return

    End Sub
    Dim errExitFuncList As Boolean = False

    Sub funcList(ByVal Mod1_File As Object)
        Console.WriteLine("./Form1.vb Filename required")
        Console.Write("./: ")

        If errExitFuncList = True Then
            Exit Sub

        End If

        Try
            Dim typeDB_File As String = "./TypeDB.txt"
            Dim Gram_File As String = "./db1_GrammarMatchedAgainstHash.txt"
            Dim CondensedGram_File As String = "./condensedGrammarParentathsised.txt"
            Dim ExpressiveGram_File As String = "./expressiveGrammarTable.txt"
            Dim funcListDB As String = "function_List_DB.txt"
            Dim fVarListDB As String = "func_Var_List_DB.txt"


            Dim sir0 As StreamReader = File.OpenText(typeDB_File)
            Dim sir1 As StreamReader = File.OpenText(Gram_File)
            Dim sir2 As StreamReader = File.OpenText(CondensedGram_File)
            Dim sir3 As StreamReader = File.OpenText(ExpressiveGram_File)
            Dim sir4 As StreamReader = File.OpenText(Mod1_File)


            Dim sue As StreamWriter = File.AppendText(funcListDB)
            Dim sueFVLDB As StreamWriter = File.AppendText(fVarListDB)



            '-------------------------------------------------
            '  CondensedGram_File Contents Example           '
            '-------------------------------------------------
            'TOKEN: Boolean AT POS. 3 & LINE:7
            'REDUCED TOKEN: System.SByte AT POS. 3 & LINE: 61
            ' (ED) dim pos 0 for future ref...
            '
            '-------------------------------------------------

            '-------------------------------------------------
            ' typeDB_File Contents Example                       '
            '-------------------------------------------------
            '    System.SByte()
            '    System.UInt16()
            '    System.UInt32()
            '    System.UInt64()
            '-------------------------------------------------

            '-------------------------------------------------
            ' Gram_File Contents Example
            '-------------------------------------------------
            'SON
            'dec var dec2 type equates string
            'Dim f As Char = "z"
            'EON
            '-------------------------------------------------

            '-------------------------------------------------
            ' ExpressiveGram_File Contents Example
            '-------------------------------------------------
            'dec var dec2 type equates string
            'R dec var dec2 type equates (number | function(var,...) | float | string)
            '-------------------------------------------------


            ' CODE GENIE CONCERNS PARENTHESIS OF FUNCTIONS
            Dim tempLine As String = ""
            Dim tempLineArrayA(100) As String
            Dim tempLineArrayB(100) As String
            Dim tempLineArrayC(100) As String
            Dim tempLineArrayD(100) As String

            Dim functionList(100) As String
            Dim funcLstIndex As Integer = 0

            Dim lineNumber As Integer = 0


            While Not sir4.EndOfStream

                lineNumber += 1
                'read file module1.vb and look for function braces, & string variable commas (Seperators)
                tempLine = sir4.ReadLine()
                tempLineArrayA = tempLine.Split("(")
                If tempLineArrayA.Length = 2 Then
                    Console.WriteLine("a. " & tempLineArrayA(0))
                    Console.WriteLine("b. " & tempLineArrayA(1))


                    tempLineArrayD = tempLineArrayA(1).Split(",")

                    Dim pos As Integer = 0

                    If tempLineArrayD.Length > 0 Then
                        For Each pin As String In tempLineArrayD
                            If Not pin Is Nothing Then
                                tempLineArrayC = pin.Split(")")
                                Console.WriteLine("c. " & tempLineArrayC(0))
                                sueFVLDB.WriteLine(lineNumber & "." & pos & "." & tempLineArrayC(0).Trim(" "))
                            End If
                            pos += 1
                        Next
                    End If






                    tempLineArrayB = tempLineArrayA(0).Split("=")
                    If tempLineArrayB.Length = 2 Then
                        tempLine = tempLineArrayB(1)
                        Console.WriteLine("1." & tempLine)
                        tempLine = tempLineArrayB(0)
                        Console.WriteLine("0." & tempLine)
                        Dim indexed As Boolean = False
                        For Each a In functionList
                            If a = tempLine Then
                                indexed = True

                            End If
                        Next
                        If indexed = False Then
                            If tempLineArrayB.Length = 2 Then
                                Try
                                    functionList(funcLstIndex) = "0." & lineNumber & " " & tempLineArrayB(0).Trim(" ")
                                    funcLstIndex += 1
                                    functionList(funcLstIndex) = "1." & lineNumber & " " & tempLineArrayB(1).Trim(" ")
                                    funcLstIndex += 1
                                Catch e As Exception
                                    Console.WriteLine(e.ToString)
                                End Try
                            End If
                        End If
                        indexed = False
                    End If

                End If
            End While
            'consider position in index of whitespace
            'associate db file with second db file of var names, something like; 1. date 1. (Now())

            ' work on graphics functions next!!!!|!


            For Each pin In functionList

                If Not pin Is Nothing Then
                    Console.Write(pin & ", ")
                    sue.Write(pin & ", ")
                End If

            Next
            sue.Write("EOF")
            If Not sue Is Nothing Then
                sue.Close()
            End If
            If Not sueFVLDB Is Nothing Then
                sueFVLDB.Close()
            End If

            If Not sir0 Is Nothing Then
                sir0.Close()
            End If
            If Not sir1 Is Nothing Then
                sir1.Close()
            End If
            If Not sir2 Is Nothing Then
                sir2.Close()
            End If
            If Not sir3 Is Nothing Then
                sir3.Close()
            End If
            If Not sir4 Is Nothing Then
                sir4.Close()
            End If
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
            errExitFuncList = True
        End Try

        Console.Write("FINISHED WITH FILE(S)")

    End Sub
    'hashtable with position(by char, by whitespace),dec (DIM) string variable (e4) dec (AS) token(date), 
    'identifer(func, type,parentheis, period, comma, +-/*=, alphanumeric) index
    Sub typeList()
        Dim hash_file As String = "./db1_GrammarMatchedAgainstHash.txt"
        Dim myStreamReader As StreamReader
        myStreamReader = File.OpenText(hash_file)

        Dim DB_File As String = "./TypeDB.txt"

        Dim siw As StreamWriter = File.AppendText(DB_File)
        Dim typeList(1000) As String
        Dim tempType(1000) As String
        'SON()
        'dec var dec2 type equates boolean
        'Dim a As Boolean = True
        'EON()
        Dim pinput As Integer = 0
        Dim node(4) As String

        While True

            node(0) = myStreamReader.ReadLine()
            node(1) = myStreamReader.ReadLine()
            node(2) = myStreamReader.ReadLine()
            node(3) = myStreamReader.ReadLine()

            If Not node(0) = "SON" And Not node(3) = "EON" Then
                Console.WriteLine("DB CORRUPT - press a key to exit")
                Console.ReadKey()
                Exit Sub
            End If

            Console.WriteLine("TOP: " & node(1))
            Console.WriteLine("BOT: " & node(2))
            tempType = node(1).Split(" ")

            Console.WriteLine("What top value do you want to index into the new file " & DB_File)

            Dim tempCounter As Integer = 0
            For Each a As String In tempType
                tempCounter += 1
                Console.WriteLine(tempCounter.ToString & ". " & a)
            Next


            pinput = Console.ReadLine()
            Dim indexedQ As Boolean = False
            While True
                tempType = node(2).Split(" ")
                For Each pin In typeList
                    If tempType(pinput - 1) = pin Then
                        indexedQ = True
                    End If
                Next
                Try
                    If indexedQ = False Then
                        typeList(tempCounter) = tempType(pinput - 1)
                        tempCounter += 1
                    End If
                    indexedQ = False
                Catch ex As Exception
                    Console.WriteLine(ex.ToString)
                End Try

                node(0) = myStreamReader.ReadLine()
                node(1) = myStreamReader.ReadLine()
                node(2) = myStreamReader.ReadLine()
                node(3) = myStreamReader.ReadLine()


                If Not node(0) = "SON" And Not node(3) = "EON" Then
                    Exit While
                End If
            End While

            For Each pin In typeList
                If Not pin Is Nothing Then
                    If pin.Length > 0 Then
                        Console.WriteLine(pin)
                        siw.WriteLine(pin)
                    End If
                End If
            Next


            If Not siw Is Nothing Then
                siw.Close()
            End If
            If Not myStreamReader Is Nothing Then
                myStreamReader.Close()
            End If


            Console.ReadLine()
            Exit While
        End While
    End Sub
    Sub parser()


        Console.WriteLine("Input File (Form1.vb)")
        Console.Write("./: ")
        Dim input_File As String = Console.ReadLine()

        Dim myStreamReader As StreamReader
        myStreamReader = File.OpenText(input_File)
        Dim array(100000) As String


        Dim line As String = myStreamReader.ReadLine
        array(0) = line
        Dim index As Integer = 1

        While Not myStreamReader.EndOfStream
            array(index) = myStreamReader.ReadLine()
            If array(index).Trim(" ").Length > 0 Then
                If array(index).Trim(" ").Substring(array(index).Trim(" ").Length - 1) = "_" Then
                    array(index) &= myStreamReader.ReadLine
                End If
            End If
            index += 1
        End While

        Dim hash(117, 21) As String
        Dim position As Integer = 0
        Dim token_Index As Integer = 0
        For Each line1 As String In array
            'parser code to generate hash table
            Dim token As String = ""
            position = 0
            If Not line1 Is Nothing Then
                For Each character As Char In line1
                    If character = "'" Then
                        Exit For
                    End If
                    token += character

                    If character = " " And token.Length > 0 Then
                        token = token.Trim(" ")
                        If token.Length > 0 Then
                            hash(token_Index, position) = token
                            'Console.WriteLine(token_Index.ToString & "TI " & position.ToString & " P " & token)
                            position += 1
                            token = ""
                        End If
                    End If
                Next
                hash(token_Index, position) = token

                position += 1
                token = ""

                token_Index += 1
            End If
        Next


        'Console.ReadLine()
        If Not myStreamReader Is Nothing Then
            myStreamReader.Close()
        End If

        'dec sv dec2 token = func/string/number
        'N a N2 b T(=) func([,])
        'N a N2 b T(=) string
        'N a N2 b T(=) number
        'N a N2 b
        '1 2 3 4 5 6
        '1234
        '123456789-n where max n is 256
        'array[21,117] (position whitespace, types)



        Dim bounce As Integer = +1
        Dim IndexTop As Integer = 116
        Dim IndexBottom As Integer = 0
        Dim ITop As Integer = 20
        Dim IBottom As Integer = 0
        Dim bouncePos As Integer = 0

        Dim IndexTokenReduced As Integer = 1000
        Dim IndexPosReduced As Integer = 21
        Dim hashReduced(IndexTokenReduced, IndexPosReduced) As String

        Dim exitLoop As Boolean = False
        Dim lastexit As Boolean = False
        Dim lostexit As Boolean = False
        Dim top As Integer = IndexTop
        Dim bottom As Integer = IndexBottom
        'double indexed array bounced in indexed circle 1,2,3,4 index for array cos(3.4127 * r^2) where r = 1234


        While Not exitLoop
            'init
            IndexTokenReduced = 0
            IndexPosReduced = 0
            lastexit = False

            While Not lastexit
                Dim tempPosition As Integer = IBottom
                IndexPosReduced = tempPosition
                lostexit = False
                Dim tempIndexReduced As Integer = 0
                While Not lostexit
                    tempIndexReduced += 1

                    If hashReduced(tempIndexReduced, IndexPosReduced) = hash(bouncePos, tempPosition) Then
                        lostexit = True
                    End If
                    If tempIndexReduced = 999 Then
                        lostexit = True
                    End If
                End While
                If hashReduced(tempIndexReduced, IndexPosReduced) = hash(bouncePos, tempPosition) Then
                    hashReduced(tempIndexReduced, IndexPosReduced) = hash(bouncePos, tempPosition)
                    'Console.WriteLine(hash(IndexBottom, tempPosition))
                    IndexTokenReduced += 1
                    If IndexTokenReduced = 1000 Then
                        lastexit = True
                    End If
                Else
                    hashReduced(bouncePos, tempPosition) = hash(bouncePos, tempPosition)
                    'Console.WriteLine(hash(bouncePos, tempPosition))
                    IndexTokenReduced += 1
                    If IndexTokenReduced = 1000 Then
                        lastexit = True
                    End If

                End If
                IBottom += 1
                If IBottom = 21 Then
                    IBottom = 0
                    bouncePos += bounce
                    If bouncePos >= top Then
                        bounce = -bounce
                        top -= 1
                    ElseIf bouncePos <= bottom Then
                        bounce = +bounce
                        bottom += 1
                    End If
                    If top = bottom Then
                        exitLoop = True
                    End If
                    If top < bottom Then
                        exitLoop = True
                    End If
                    Dim tempToken As String = hash(bouncePos, IBottom)

                End If

            End While

        End While

        'Console.ReadLine()
        Dim ConGramFile As String = "./condensedGrammarParentathsised.txt"
        Dim myStreamWriter As StreamWriter
        myStreamWriter = File.AppendText(ConGramFile)

        For x As Integer = 0 To 116
            For y As Integer = 0 To 20
                If Not hash(x, y) Is Nothing Then
                    If hash(x, y).Length > 0 Then
                        myStreamWriter.WriteLine("TOKEN: " & hash(x, y) & " AT POS. " & y & " & LINE:" & x)
                    End If
                End If
            Next
        Next
        myStreamWriter.Flush()

        For x As Integer = 0 To 116
            For y As Integer = 0 To 20
                If Not hashReduced(x, y) Is Nothing Then
                    If hashReduced(x, y).Length > 0 Then
                        myStreamWriter.WriteLine("REDUCED TOKEN: " & hashReduced(x, y) & " AT POS. " & y & " & LINE: " & x)
                    End If
                End If
            Next
        Next
        myStreamWriter.Flush()

        If Not myStreamWriter Is Nothing Then
            myStreamWriter.Close()
        End If


        Dim ExpGramTable_File As String = "./expressiveGrammarTable.txt"

        Dim sw As StreamWriter = File.CreateText(ExpGramTable_File)
        'associative/substitutive grammar
        sw.WriteLine("dec var")
        sw.WriteLine("dec var dec2 type")
        sw.WriteLine("dec var dec2 type equates number")
        sw.WriteLine("dec var dec2 type equates function(var,...)")
        sw.WriteLine("dec var dec2 type equates float")
        sw.WriteLine("dec var dec2 type equates string")
        sw.WriteLine("R dec var dec2 type equates (number | function(var,...) | float | string)")

        sw.Flush()
        If Not sw Is Nothing Then
            sw.Close()
        End If

        'If conten = "dec" Then
        '    output("Dim")
        'End If

        'If sw.readline("") position 1 then output array(0,1) to array(116,1)


        'hash(tokenIndex, position)
        Dim sir As StreamReader
        sir = File.OpenText(ExpGramTable_File)
        Dim eGrammar(100) As String
        Dim eGrammarIndex As Integer = 0

        While Not sir.EndOfStream
            eGrammar(eGrammarIndex) = sir.ReadLine()
            Console.WriteLine(eGrammar(eGrammarIndex))
            eGrammarIndex += 1
        End While


        Dim GramHash_File As String = "./db1_GrammarMatchedAgainstHash.txt"


        Dim siw As StreamWriter
        siw = File.AppendText(GramHash_File)
        Dim tolken As String = ""
        Dim input As Integer
        Dim tolkenIndex As Integer = 0
        Dim pos As Integer = 0
        While True
            Console.Clear()
            While True

                Console.WriteLine("0. read 1. skip 2. node complete 3. start 4. exit sub 5. next token 6. next 7. goto")
                Console.WriteLine("TOLKEN: " & tolkenIndex.ToString & " POS: " & pos.ToString)
                Console.WriteLine(hash(tolkenIndex, pos))
                input = Console.ReadLine()

                If input = 0 Then
                    If pos = 0 Then
                        tolken &= hash(tolkenIndex, pos)
                    Else
                        tolken &= " " & hash(tolkenIndex, pos)
                    End If

                End If
                If input = 1 Then
                    tolkenIndex += 1
                End If
                If input = 2 Then
                    Exit While
                End If
                If input = 3 Then
                    tolken = ""
                    pos = 0
                End If

                If input = 4 Then


                    If Not siw Is Nothing Then
                        siw.Close()
                    End If

                    If Not sir Is Nothing Then
                        sir.Close()
                    End If

                    Exit Sub
                End If
                If input = 5 Then
                    tolkenIndex += 1
                    pos = 0
                    If tolkenIndex = 117 Then
                        Console.WriteLine("Exceeded EO\f")
                    End If
                End If
                If input = 6 Then
                    pos += 1
                    If pos = 21 Then
                        pos = 20
                        Console.WriteLine("Exceeded Array UBounds")
                    End If
                End If
                If input = 7 Then
                    Console.WriteLine("INPUT TOLKEN NO.:")
                    input = Console.ReadLine()
                    tolkenIndex = input
                End If
            End While






            Console.Clear()

            Console.WriteLine("0. dec var")
            Console.WriteLine("1. dec var dec2 type")
            Console.WriteLine("2. dec var dec2 type equates Float")
            Console.WriteLine("3. dec var dec2 type equates String")
            Console.WriteLine("4. dec var dec2 type equates Integral Number")
            Console.WriteLine("5. dec var dec2 type equates function(var,...)")
            Console.WriteLine("6. dec var dec2 type equates Expression")
            Console.WriteLine("7. dec var dec2 type equates OBJECT")
            Console.WriteLine("8. dec var dec2 type equates boolean")

            Console.WriteLine("10. exit sub")

            Console.WriteLine(tolken)
            input = Console.ReadLine()
            If input = 10 Then
                Exit While
            End If


            If input = 0 Then
                siw.WriteLine("SON")
                siw.WriteLine("dec var")
                siw.WriteLine(tolken)
                siw.WriteLine("EON")
            End If

            If input = 1 Then
                siw.WriteLine("SON")
                siw.WriteLine("dec var dec2 type")
                siw.WriteLine(tolken)
                siw.WriteLine("EON")
            End If

            If input = 2 Then
                siw.WriteLine("SON")
                siw.WriteLine("dec var dec2 type equates float")
                siw.WriteLine(tolken)
                siw.WriteLine("EON")
            End If

            If input = 3 Then
                siw.WriteLine("SON")
                siw.WriteLine("dec var dec2 type equates string")
                siw.WriteLine(tolken)
                siw.WriteLine("EON")
            End If

            If input = 4 Then
                siw.WriteLine("SON")
                siw.WriteLine("dec var dec2 type equates number")
                siw.WriteLine(tolken)
                siw.WriteLine("EON")
            End If

            If input = 5 Then
                siw.WriteLine("SON")
                siw.WriteLine("dec var dec2 type equates function")
                siw.WriteLine(tolken)
                siw.WriteLine("EON")
            End If

            If input = 6 Then
                siw.WriteLine("SON")
                siw.WriteLine("dec var dec2 type equates expression")
                siw.WriteLine(tolken)
                siw.WriteLine("EON")
            End If

            If input = 7 Then
                siw.WriteLine("SON")
                siw.WriteLine("dec var dec2 type equates object")
                siw.WriteLine(tolken)
                siw.WriteLine("EON")
            End If

            If input = 8 Then
                siw.WriteLine("SON")
                siw.WriteLine("dec var dec2 type equates boolean")
                siw.WriteLine(tolken)
                siw.WriteLine("EON")
            End If


        End While


        If Not siw Is Nothing Then
            siw.Close()
        End If

        If Not sir Is Nothing Then
            sir.Close()
        End If


    End Sub
End Module
Class node
    Private CPos As Integer

    Public CharPosition As Integer

    Private Node As String = Nothing

    Public NodeItem As String = Nothing

    Public Line As Integer = 0
    Public Sub Insert(ByVal NodeItem As String)
        Node = NodeItem

    End Sub
    Public Function Extract() As String
        NodeItem = Node
        Return NodeItem

    End Function

    Public Sub SetPosition(ByVal CharPosition As Integer)
        CPos = CharPosition
    End Sub

    'Return Start Position of Node's List Item
    Public Function returnPosition() As Integer
        CharPosition = CPos
        Return CharPosition
    End Function

End Class
Module Module1
    'Hash_Table produces HTML Comment Files, Based on User Input
    Public TempStringLine As String
    Public ArrayCB(1000) As String
    Public CharArray(50) As Char
    Public Nest(100, 2) As Integer
    'nest stored index line numbers start(x,0) and finish(x,1)
    Sub Main()
        Dim CommandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String) = My.Application.CommandLineArgs
        'For i As Integer = 0 To CommandLineArgs.Count - 1
        'MsgBox(CommandLineArgs(i))
        'Next
        If CommandLineArgs.Count = 0 Or CommandLineArgs.Count > 1 Then
            Console.WriteLine("*****************************************")
            Console.WriteLine("* Hash-Table.exe <Form1.vb>")
            Console.WriteLine("* Please Enter a Filename for File Analysis")
            Console.WriteLine("*****************************************")
            Console.WriteLine("Press Any Key to Exit...")
            Console.ReadKey()
            Exit Sub
        End If
        REM **************************************************************************
        Dim fileNom As String = CommandLineArgs(0)
        Dim ioStream As IO.StreamReader = FileIO.FileSystem.OpenTextFileReader(fileNom)
        Dim stringArray(1000) As String
        Dim index As Integer = 0
        REM How-To: Open File (Code Block)
        REM **************************************************************************
        While Not ioStream.EndOfStream
            stringArray(index) = ioStream.ReadLine
            index += 1
        End While
        REM How-TO: Read File (Complete Contents Code Block) To String Array
        REM **************************************************************************
        'reset index + Store Index Length
        'Store Length
        Dim Sum As Integer = index
        index = 0
        REM END OF RESET + STORE ARRAY LENGTH
        REM **************************************************************************
        For index = 0 To Sum
            'Perform Recursive Analysis on String(s)
            TempStringLine = stringArray(index)
            'TempStringLine Mighten Be Empty Due to Index Off By One Error
            'Hence the Error Checking IF Statement
            If Not TempStringLine = Nothing Then
                'Now TempStringLine Contains a String, Query TempStringLine Using Search()
                ' Search is a series of Subs and Functions Abstracted Out to Single Search
                ' Single Search Term Queries
                Search("If")
                Search("Else")
                Search("For")
                Search("Dim")
                Search("While")
                Search("Function")
                Search("Sub")
                Search("Return")
                Search("End Sub")
                Search("End If")
                Search("End Function")
                Search("End While")
                Search("Next")
                Search("Do")
                Search("Loop")
                Search("Until")
                ' The above search terms, (IF, FOR, ELSE Etc.)
                ' Are VB.NET Commands that are commonly used in programming in VB.NET
            End If
        Next
        REM **************************************************************************
        Console.WriteLine("Please Press Any Key...")
        Console.ReadKey()
        'Wait for Key Press so we can review data
        REM **************************************************************************

    End Sub

    REM **************************************************************************
    REM Code Block-Search Abstraction
    Function SubSearch(ByVal Search_Term As String, ByVal Search_String As String) As Boolean
        'Query .Contains For RegEx Match on Search Term
        ' Ps, .Contains does not accept RegEx statements! Just String
        If Search_String.Contains(Search_Term) = True Then
            Return True
        Else
            Return False
        End If
    End Function
    ' Search-Abstractions
    Sub SubSearchAbs(ByVal Search_Term As String, ByVal Search_String As String)
        If SubSearch(Search_Term, Search_String) = True Then
            Console.WriteLine(Search_Term)
            'visit 1 for attributes
            'read all chars of string line into 50 digit array
            ' mark spaces in node tree
            'if code block (Such as IF Statement)
            ' collect all lines of code block into arrayCB
            'collect binary Tree of Code Blocks
            ' For Nested Conditionals Statements

        End If
    End Sub
    ' Final Abstraction Layer of Seatch taking call to one term (SearchString)
    ' TempStringLine is Global Declared at top of Document as 'Public TempStringLine As String'
    Sub Search(ByVal SearchString As String)
        SubSearchAbs(SearchString, TempStringLine)
    End Sub
    REM End Of Code Block for Search Abstraction
    REM **************************************************************************
End Module
